<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/models_import.php");

// Customer
// $customer = new Customer("user", "pass", "images/im1.jpg");
// $err = $customer->intoDb();
// echo $err;

// $customer = Customer::fromDb(9);
// var_dump($customer);


// Category
// $cat = new Category("Phone");
// $err = $cat->intoDb();
// echo $err;

// $cat = Category::fromDb(1);
// var_dump($cat);


// Item
// $item = new Item("Xiaomi Redmi 9", 1);
// $err = $item->intoDb();
// echo $err;

// $item = Item::fromDb(1);
// var_dump($item);


// Image
// $im = new Image("images/im2.jpg", 1);
// $err = $im->intoDb();
// echo $err;

// $im = Image::fromDb(1);
// var_dump($im);


// Sale
// $sale = new Sale(1, 9, 2);
// $err = $sale->intoDb();
// echo $err;

// $sale = Sale::fromDb(1);
// var_dump($sale);
