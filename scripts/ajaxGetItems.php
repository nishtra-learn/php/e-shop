<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Item.php");

$categoryId = $_POST["category"];
$items = Item::getDbEntries($categoryId, 0, true);
$htmlResponse = "";

foreach ($items as $key => $item) {
    $htmlResponse .= $item->buildCatalogItem();
}

echo $htmlResponse;