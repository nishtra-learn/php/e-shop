<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/ValidationService.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Item.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Image.php");

session_start();

$itemName = trim($_POST["itemName"]);
$category = intval(trim($_POST["category"]));
$price = doubleval(trim($_POST["price"]));
$salePrice = doubleval(trim($_POST["salePrice"]));
$itemInfo = trim($_POST["itemInfo"]);
$images = $_FILES["uploadImages"];

// validate here
$errors = ValidationService::itemAddForm($itemName, $itemInfo, $images);

if (count($errors) == 0) {
    $item = new Item($itemName, $category, $price, $salePrice, $info);
    $itemDbErr = $item->intoDb();

    if ($itemDbErr == 0) {
        // add images
        foreach ($images["name"] as $key => $value) {
            if ($images["error"][$key] != UPLOAD_ERR_OK)
                continue;
            
            $filename = $images["name"][$key];
            $pathParts = pathinfo($filename);
            $filename = $pathParts["filename"] . time() . "." . $pathParts["extension"];
            $path = "images/" . $filename;
            $fullpath = $_SERVER["DOCUMENT_ROOT"] . "/" . $path;
            
            $fileMoveSuccess = move_uploaded_file($images["tmp_name"][$key], $fullpath);
            if ($fileMoveSuccess) {
                $image = new Image($path, $item->id);
                $image->intoDb();
            }
        }

        $_SESSION[SES_SUCCESS] = "Item successfully added";
    }
    else {
        $_SESSION[SES_ERROR]["message"] = $itemDbErr;
    }
}
else {
    $_SESSION[SES_ERROR] = $errors;
}

Helpers::headerRedirect($_SERVER["HTTP_REFERER"]);
exit();