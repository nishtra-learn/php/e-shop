<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/page_map.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/ValidationService.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Customer.php");

if ($_POST["regsubmit"]) {
    session_start();

    $login = $_POST["login"];
    $pass = $_POST["pass"];
    $passConfirm = $_POST["passConfirm"];
    $userpicFile = $_FILES["uploadUserpic"];

    $errors = ValidationService::registrationForm($login, $pass, $passConfirm, $userpicFile);
    
    if (count($errors) == 0) {
        // try saving userpic file if it was uploaded
        $fileSaveSuccess = null;
        $filepath = null;
        if ($userpicFile["error"] != UPLOAD_ERR_NO_FILE) {
            $filename = $userpicFile["name"];
            $pathParts = pathinfo($filename);
            $filename = $pathParts["filename"] . time() . "." . $pathParts["extension"];
            $filepath = "images/" . $filename;
            $filesystemPath = $_SERVER["DOCUMENT_ROOT"] . "/" . $filepath;
            
            $fileSaveSuccess = move_uploaded_file($userpicFile["tmp_name"], $filesystemPath);

            if (!$fileSaveSuccess) {
                $errors["fileSave"] = "Userpic file couldn't be saved";
            }
        }

        // if userpic uploaded, try writing user to the DB only if userpic was successfully saved
        if ($fileSaveSuccess || $userpicFile["error"] == UPLOAD_ERR_NO_FILE) {
            $err = Helpers::registerUser($login, $pass, $filepath);
                
            if ($err !== 0) {
                if ($err === 1062) {
                    $errors["db"] = "User with this login already exists";
                }
                else {
                    $errors["db"] = "Error encountered when querying the database. \"$err\"";
                }
                
                // delete saved userpic file
                unlink($filesystemPath);
            }
        }
    }
    
    if (count($errors) == 0) {
        $_SESSION[SES_SUCCESS] = $login;
    }
    else {
        $_SESSION[SES_ERROR]["errors"] = $errors;
        $_SESSION[SES_ERROR]["login"] = $login;
    }
}

$redirectUri = "/index.php?page=" . PageMap::REGISTRATION;
header("Location: $redirectUri", true, 303);
exit();