<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Item.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");

session_start();

$ruser = $_SESSION[SES_RUSER] ?? $_SESSION[SES_RADMIN] ?? "cart";
$ruser = rawurlencode($ruser);
$ruserId = $_SESSION[SES_RUSER_ID] ?? null;
$cookieCartItems = [];
foreach ($_COOKIE as $cookieName => $value) {
    $tokens = explode("_", $cookieName);
    
    if ($tokens[0] != $ruser)
        continue;
        
    $itemid = $tokens[1];
    $count = $value;
    
    $item = Item::fromDb($itemid);
    $item->sell($ruserId, $count);

    unset($_COOKIE[$cookieName]);
    $res = setcookie($cookieName, "0", time() - 1000, "/");
}

$redirectPath = "/index.php?page=" . PageMap::CART;
Helpers::headerRedirect($redirectPath);
exit();