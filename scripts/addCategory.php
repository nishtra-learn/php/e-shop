<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Category.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");
session_start();

$categoryName = $_POST["categoryName"];
$err = null;
if (trim($categoryName) == "") {
    $err = "This field is required";
    $_SESSION[SES_ERROR] = $err;
}
// validate more here

if ($err == null) {
    $category = new Category($categoryName);
    $err = $category->intoDb();
    if ($err !== 0) {
        $_SESSION[SES_ERROR]["category"] = $err;
    }
    else {
        $_SESSION[SES_SUCCESS] = "Category successfully added";
    }
}

Helpers::headerRedirect($_SERVER["HTTP_REFERER"]);
exit();