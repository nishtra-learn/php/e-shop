<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Item.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");

session_start();

$ruser = $_SESSION[SES_RUSER] ?? $_SESSION[SES_RADMIN] ?? "cart";
$ruser = rawurlencode($ruser);

$htmlResponse = "";
$totalCost = 0;

$sortedCookies = $_COOKIE;
ksort($sortedCookies, SORT_NATURAL);
foreach ($sortedCookies as $cookieName => $value) {
    $tokens = explode("_", $cookieName);
    
    if ($tokens[0] != $ruser)
        continue;
        
    $itemid = $tokens[1];
    $count = $value;
    
    $item = Item::fromDb($itemid, true);
    $htmlResponse .= $item->buildCartItem($count);
    $totalCost += $item->salePrice * $count;
}

$responseObj = array(
    "html" => $htmlResponse,
    "total" => $totalCost
);
$response = json_encode($responseObj);

echo $response;
exit();