let catalogCategorySelect = document.querySelector("#catalog #category");
let catalogItemsContainer = document.querySelector("#catalog-items");
catalogCategorySelect?.addEventListener("change", (e) => {
    let catId = e.target.value;
    let formData = new FormData();
    formData.append("category", catId);
    catalogItemsContainer.innerHTML = "";
    fetch("/scripts/ajaxGetItems.php", { method: "post", body: formData })
        .then(response => response.text())
        .then(content => {
            catalogItemsContainer.innerHTML = content;
        })
});


 document.querySelectorAll(".catalog-item .btnAdd").forEach((elem) => {
     elem.addEventListener("click", () => {
        let itemId = elem.querySelector("input[type=hidden]").value;
        let cookieObj = [];
        document.cookie.split("; ").map((el) => {
            let kvp = el.split("=");
            cookieObj[kvp[0]] = kvp[1];
        })
        let username = cookieObj["ruser"] !== "-" ? cookieObj["ruser"] : "cart";
        
        // make cookie
        let cookieName = username + "_" + itemId;
        if (!cookieObj[cookieName]) {
            let date = new Date(new Date().getTime() + 60 * 1000 * 60);
            document.cookie = cookieName + "=" + 1 + "; path=/; expires=" + date.toUTCString();
        }
     });
 });


async function refreshShoppingCart() {
    await fetch("/scripts/ajaxGetCartItems.php", {method: "post"})
    .then(response => response.json())
    .then(content => {
        document.querySelector("#cart-items table").innerHTML = content["html"];
        document.querySelector("#order .totalCost .totalCostValue").innerHTML = content["total"];
    });
}


document.querySelector("#cart-items")?.addEventListener("click", (e) => {
    e.stopPropagation();
    let bubblePath = [...e.path];
    let cartItemBtn = bubblePath.filter(el => {
        if (el.classList)
            return el.classList.contains("cartItemBtn");
        else 
            return false;
    });

    // event bubbled through .cartItemBtn, meaning that it came either from button or it's nested elements
    if (cartItemBtn.length == 0)
        return;

    let btn = cartItemBtn[0];
    let itemId = btn.parentElement.querySelector(".itemid").value;
    let cookieObj = [];
    document.cookie.split("; ").map((el) => {
        let kvp = el.split("=");
        cookieObj[kvp[0]] = kvp[1];
    })
    let username = cookieObj["ruser"] !== "-" ? cookieObj["ruser"] : "cart";
    let cookieName = username + "_" + itemId;
    
    if (btn.classList.contains("btnDel")) {
        // remove cookie
        let date = new Date(0);
        document.cookie = cookieName + "=" + 1 + "; path=/; expires=" + date.toUTCString();
    }
    else {
        let itemCount = cookieObj[cookieName];
        if (btn.classList.contains("btnPlus")) {
            itemCount++;
        }
        else if (btn.classList.contains("btnMinus") && itemCount > 1) {
            itemCount--;
        }
        document.cookie = cookieName + "=" + itemCount;
    }

    refreshShoppingCart();
});
