<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");

class Role {
    public $id;
    public $role;
    
    public function __construct($roleName, $id = 0) {
        $this->id = $id;
        $this->role = $roleName;
    }

    /**
     * Write object to DB
     * @return int|string Returns an error message on error. 0 is returned on success.
     */
    public function intoDb() {
        $pdo = Helpers::connect();
        try {
            $arr = (array)$this;
            array_shift($arr);
            $sql = "INSERT INTO Roles(`role`) 
                VALUES (:role)";
            $ps = $pdo->prepare($sql);
            $ps->execute($arr);
            $this->id = $pdo->lastInsertId();
            return 0;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Get entity with the specified ID from the DB
     * @return Role|null|false Returns found entity on success and false on failure.
     */
    public static function fromDb($id) {
        $role = null;
        try {
            $pdo = Helpers::connect();
            $ps = $pdo->prepare("SELECT * FROM Roles WHERE id = ?");
            $ps->execute(array($id));
            if ($row = $ps->fetch()) {
                $role = new Role($row["role"], $row["id"]);
            }
            return $role;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }
}