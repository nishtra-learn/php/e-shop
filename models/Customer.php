<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");

class Customer {
    public $id;
    public $login;
    public $password;
    public $roleid;
    public $imagepath;
    public $discount;
    public $total;

    public function __construct($login, $password, $imagepath, $id = 0) {
        $this->login = $login;
        $this->password = $password;
        $this->imagepath = $imagepath;
        $this->id = $id;
        $this->roleid = ROLE_ID_CUSTOMER;
        $this->discount = 0;
        $this->total = 0;
    }

    /**
     * Write object to DB
     * @return int|string Returns code 1062 if unique constraint was violated when inserting values
     * or error message on all other exceptions. 0 is returned on success.
     */
    public function intoDb() {
        $pdo = Helpers::connect();
        try {
            $arr = (array)$this;
            array_shift($arr);
            $sql = "INSERT INTO Customers(`login`, `password`, `roleid`, `imagepath`, `discount`, `total`) 
                VALUES (:login, :password, :roleid, :imagepath, :discount, :total)";
            $ps = $pdo->prepare($sql);
            $ps->execute($arr);
            $this->id = $pdo->lastInsertId();
            return 0;
        } catch (PDOException $ex) {
            if ($ex->getCode() == "23000")
                return 1062;
            else 
                return $ex->getMessage();
        }
    }

    /**
     * Get entity with the specified ID from the DB
     * @return Customer|null|false Returns found entity on success and false on failure.
     */
    public static function fromDb($id) {
        $customer = null;
        try {
            $pdo = Helpers::connect();
            $ps = $pdo->prepare("SELECT * FROM Customers WHERE id = ?");
            $ps->execute(array($id));
            if ($row = $ps->fetch()) {
                $customer = new Customer($row["login"], $row["password"], $row["imagepath"], $row["id"]);
                $customer->total = $row["total"];
                $customer->discount = $row["discount"];
                $customer->roleid = $row["roleid"];
            }
            return $customer;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public static function authenticate($login, $password) {
        $passHash = md5($password);

        $pdo = Helpers::connect();
        $sql = "SELECT * FROM Customers WHERE `login` = :login AND `password` = :password";
        $ps = $pdo->prepare($sql);
        $res = $ps->execute(array("login" => $login, "password" => $passHash));
        
        if ($res === true && $row = $ps->fetch()) {
            $user = new Customer($row["login"], $row["password"], $row["imagepath"], $row["id"]);
            $user->roleid = $row["roleid"];
            $user->discount = $row["discount"];
            $user->total = $row["total"];

            return $user;
        }
        else {
            return null;
        }
    }
}
