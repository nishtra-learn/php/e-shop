<?php
// id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
// category varchar(32) NOT NULL UNIQUE

include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");

class Category {
    public $id;
    public $category;
    
    public function __construct($categoryName, $id = 0) {
        $this->id = $id;
        $this->category = $categoryName;
    }

    /**
     * Write object to DB
     * @return int|string Returns error message on error. 0 is returned on success.
     */
    public function intoDb() {
        $pdo = Helpers::connect();
        try {
            $arr = (array)$this;
            array_shift($arr);
            $sql = "INSERT INTO Categories(`category`) 
                VALUES (:category)";
            $ps = $pdo->prepare($sql);
            $ps->execute($arr);
            $this->id = $pdo->lastInsertId();
            return 0;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Get entity with the specified ID from the DB
     * @return Category|null|false Returns found entity on success and false on failure.
     */
    public static function fromDb($id) {
        $category = null;
        try {
            $pdo = Helpers::connect();
            $ps = $pdo->prepare("SELECT * FROM Categories WHERE id = ?");
            $ps->execute(array($id));
            if ($row = $ps->fetch()) {
                $category = new Category($row["category"], $row["id"]);
            }
            return $category;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public static function getDbEntries($limit = 0) {
        $entries = [];
        try {
            $pdo = Helpers::connect();
            $sql = "SELECT * FROM Categories";
            $param = [];
            if ($limit > 0) {
                $sql .= " LIMIT ?";
                $param[] = $limit;
            }
            $ps = $pdo->prepare($sql);
            $ps->execute($param);
            $rows = $ps->fetchAll();
            foreach ($rows as $row) {
                $entries[] = new Category($row["category"], $row["id"]);
            }
            return $entries;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }
}