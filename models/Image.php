<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");

class Image {
    public $id;
    public $itemid;
    public $imagepath;
    
    public function __construct($imagepath, $itemid, $id = 0) {
        $this->imagepath = $imagepath;
        $this->itemid = $itemid;
        $this->id = $id;
    }

    /**
     * Write object to DB
     * @return int|string Returns an error message on error. 0 is returned on success.
     */
    public function intoDb() {
        $pdo = Helpers::connect();
        try {
            $arr = (array)$this;
            array_shift($arr);
            $sql = "INSERT INTO Images(`itemid`, `imagepath`) 
                VALUES (:itemid, :imagepath)";
            $ps = $pdo->prepare($sql);
            $ps->execute($arr);
            $this->id = $pdo->lastInsertId();
            return 0;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Get entity with the specified ID from the DB
     * @return Image|null|false Returns found entity on success and false on failure.
     */
    public static function fromDb($id) {
        $image = null;
        try {
            $pdo = Helpers::connect();
            $ps = $pdo->prepare("SELECT * FROM Images WHERE id = ?");
            $ps->execute(array($id));
            if ($row = $ps->fetch()) {
                $image = new Image($row["imagepath"], $row["itemid"], $row["id"]);
            }
            return $image;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public static function getDbEntries($itemId = 0, $limit = 0) {
        $entries = [];
        try {
            $pdo = Helpers::connect();
            $param = [];
            $sql = "SELECT * FROM Images";
            if ($itemId > 0) {
                $sql .= " WHERE itemid = :itemid";
                $param["itemid"] = $itemId;
            }
            if ($limit > 0) {
                $sql .= " LIMIT :limit";
                $param["limit"] = $limit;
            }

            $ps = $pdo->prepare($sql);
            $ps->execute($param);
            $rows = $ps->fetchAll();
            foreach ($rows as $row) {
                $entries[] = new Image($row["imagepath"], $row["itemid"], $row["id"]);
            }
            return $entries;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }
}