<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/page_map.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Image.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/Sale.php");

class Item {
    public $id;
    public $itemName;
    public $categoryid;
    public $price;
    public $salePrice;
    public $info;
    public $rating;
    public $action;

    // referenced values
    public $category;
    public $images;
    
    public function __construct($itemName, $categoryId, $price = 0, $salePrice = 0, $info = "", $rating = 0, $action = 0, $id = 0) {
        $this->itemName = $itemName;
        $this->categoryid = $categoryId;
        $this->id = $id;
        $this->price = $price;
        $this->salePrice = $salePrice;
        $this->info = $info;
        $this->rating = $rating;
        $this->action = $action;

        $this->category = null;
        $this->images = [];
    }

    /**
     * Write object to DB
     * @return int|string Returns an error message on error. 0 is returned on success.
     */
    public function intoDb() {
        $pdo = Helpers::connect();
        try {
            $arr = (array)$this;
            //array_shift($arr);
            $arr = array_slice($arr, 1, 7);
            $sql = "INSERT INTO Items(`itemName`, `categoryid`, `price`, `salePrice`, `info`, `rating`, `action`) 
                VALUES (:itemName, :categoryid, :price, :salePrice, :info, :rating, :action)";
            $ps = $pdo->prepare($sql);
            $ps->execute($arr);
            $this->id = $pdo->lastInsertId();
            return 0;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Get entity with the specified ID from the DB
     * @return Item|null|false Returns found entity on success and false on failure.
     */
    public static function fromDb($id, $withReferencedValues = false) {
        $item = null;
        try {
            $pdo = Helpers::connect();
            $sql = "SELECT * FROM Items WHERE id = ?";
            if ($withReferencedValues) {
                $sql = "SELECT items.id, `itemName`, `categoryid`, category, `price`, `salePrice`, `info`, `rating`, `action` 
                FROM `items` 
                JOIN categories on categories.id = items.categoryid
                WHERE items.id = ?";
            }
            $ps = $pdo->prepare($sql);
            $ps->execute(array($id));
            if ($row = $ps->fetch()) {
                $item = new Item(
                    $row["itemName"], $row["categoryid"], $row["price"], 
                    $row["salePrice"], $row["info"], $row["rating"], 
                    $row["action"], $row["id"]);
                
                if ($withReferencedValues) {
                    $item->category = $row["category"] ?? null;
                    $item->images = Image::getDbEntries($item->id);
                }
            }
            return $item;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public static function getDbEntries($categoryId = 0, $limit = 0, $withReferencedValues = false) {
        $entries = [];
        try {
            $pdo = Helpers::connect();
            $param = [];
            $sql = "SELECT * FROM Items";
            if ($withReferencedValues) {
                $sql = "SELECT items.id, `itemName`, `categoryid`, category, `price`, `salePrice`, `info`, `rating`, `action` 
                    FROM `items` 
                    JOIN categories on categories.id = items.categoryid";
            }
            if ($categoryId > 0) {
                $sql .= " WHERE categoryid = :categoryid";
                $param["categoryid"] = $categoryId;
            }
            if ($limit > 0) {
                $sql .= " LIMIT :limit";
                $param["limit"] = $limit;
            }

            $ps = $pdo->prepare($sql);
            $ps->execute($param);
            $rows = $ps->fetchAll();
            foreach ($rows as $row) {
                $item = new Item(
                    $row["itemName"], $row["categoryid"], $row["price"], 
                    $row["salePrice"], $row["info"], $row["rating"], 
                    $row["action"], $row["id"]);
                
                if ($withReferencedValues) {
                    $item->category = $row["category"] ?? null;
                    $item->images = Image::getDbEntries($item->id);
                }

                $entries[] = $item;
            }
            return $entries;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public function buildCatalogItem() {
        $imagepath = isset($this->images[0]) ? $this->images[0]->imagepath : "/images/noimage.png";
        ob_start();
        ?>
        <div class="card catalog-item">
            <div class="img-container">
                <img src="<?= $imagepath ?>" class="card-img-top" alt="...">
            </div>
            <div class="card-body d-flex flex-column">
                <h5 class="card-title">
                    <a href="/index.php?page=<?= PageMap::ITEM ?>&item=<?= $this->id ?>"><?= $this->itemName ?></a>
                </h5>
                <h5 class="item-price text-success"><?= $this->salePrice ?> UAH</h5>
                <div class="d-flex align-items-end flex-grow-1 justify-content-end">
                    <button class="btn btn-success btnAdd">
                        <span>Add to cart</span>
                        <!-- store item id to use when adding it to the cart -->
                        <input type="hidden" value="<?= $this->id ?>"> 
                    </button>
                </div>
            </div>
        </div>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    public function buildCartItem($count) {
        ob_start();
        ?>
        <tr>
            <td>
                <div class="img-container">
                    <?php if (isset($this->images[0]->imagepath)) : ?>
                        <img src="<?= $this->images[0]->imagepath ?>" alt="">
                    <?php endif ?>
                </div>
            </td>
            <td>
                <h5><?= $this->itemName ?></h5>
            </td>
            <td>
                <h5><?= $this->salePrice ?></h5>
            </td>
            <td>
                <h5>x<?= $count ?></h5>
            </td>
            <td>
                <input type="hidden" class="itemid" value="<?= $this->id ?>">
                <button class="btn btn-outline-primary btn-sm cartItemBtn btnMinus">
                    <i class="fas fa-minus"></i>
                </button>
                <button class="btn btn-outline-primary btn-sm cartItemBtn btnPlus">
                    <i class="fas fa-plus"></i>
                </button>
                <button class="btn btn-danger btn-sm cartItemBtn btnDel">
                    <i class="fas fa-times"></i>
                </button>
            </td>
        </tr>
        <?php
        $html = ob_get_clean();
        return $html;
    }


    public function sell($customerid, $count) {
        if ($customerid > 0) {
            $pdo = Helpers::connect();
            $sql = "UPDATE `customers` SET `total`=`total` + (:price * :count) WHERE id = :id";
            $ps = $pdo->prepare($sql);
            $customerUpdateRes = $ps->execute(array("price" => $this->salePrice, "count" => $count, "id" => $customerid));

            $sale = new Sale($this->id, $customerid, $count);
            $saleRes = $sale->intoDb();
        }
        else {
            $sale = new Sale($this->id, null, $count);
            $saleRes = $sale->intoDb();
        }
    }
}