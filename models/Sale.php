<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/helpers.php");

class Sale {
    public $id;
    public $itemid;
    public $customerid;
    public $quantity;
    public $date;
    
    public function __construct($itemid, $customerid, $quantity, $date = null, $id = 0) {
        $this->itemid = $itemid;
        $this->customerid = $customerid;
        $this->quantity = $quantity;
        $this->date = $date ?? date("Y-m-d", time());
        $this->id = $id;
    }

    /**
     * Write object to DB
     * @return int|string Returns an error message on error. 0 is returned on success.
     */
    public function intoDb() {
        $pdo = Helpers::connect();
        try {
            $arr = (array)$this;
            array_shift($arr);
            $sql = "INSERT INTO Sales(`itemid`, `customerid`, `quantity`, `date`) 
                VALUES (:itemid, :customerid, :quantity, :date)";
            $ps = $pdo->prepare($sql);
            $ps->execute($arr);
            $this->id = $pdo->lastInsertId();
            return 0;
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Get entity with the specified ID from the DB
     * @return Sale|null|false Returns found entity on success and false on failure.
     */
    public static function fromDb($id) {
        $sale = null;
        try {
            $pdo = Helpers::connect();
            $ps = $pdo->prepare("SELECT * FROM Sales WHERE id = ?");
            $ps->execute(array($id));
            if ($row = $ps->fetch()) {
                $sale = new Sale($row["itemid"], $row["customerid"], $row["quantity"], $row["date"], $row["id"]);
            }
            return $sale;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }
}