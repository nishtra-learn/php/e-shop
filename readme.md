Basic partial implementation of e-shop in pure PHP

Includes
1. Authentication
2. CRUD for products (at least C(reate) part)
3. Shopping cart implemented using cookies

Doesn't include any kind of payment processing
