<?php
$categories = Category::getDbEntries();
$err = null;
if ($categories == false) {
    $err = "DB request fail";
}

$items = Item::getDbEntries(0, 0, true);
?>

<h3>Catalog</h3>

<?php if (isset($err)): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $err ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif ?>

<div id="catalog">
    <select name="category" id="category" class="form-control">
        <option value="0">All</option>
        <?php foreach ($categories as $cat): ?>
            <option value="<?= $cat->id ?>">
                <?= $cat->category ?>
            </option>
        <?php endforeach ?>
    </select>

    <div id="catalog-items">
        <?php foreach ($items as $key => $value) {
            echo $value->buildCatalogItem();
        }
        ?>
    </div>
</div>