<?php
$error = "Unexpected error encountered!";
if (isset($_SESSION[SES_ERROR])) {
    $error = $_SESSION[SES_ERROR];
}
?>

<span><?php echo $error ?></span>

<?php
unset($_SESSION[SES_ERROR]);
?>