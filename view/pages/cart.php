<?php
$ruser = $_SESSION[SES_RUSER] ?? $_SESSION[SES_RADMIN] ?? "cart";
$ruser = rawurlencode($ruser);

$items = [];
$totalCost = 0;

$sortedCookies = $_COOKIE;
ksort($sortedCookies, SORT_NATURAL);
foreach ($sortedCookies as $cookieName => $value) {
    $tokens = explode("_", $cookieName);
    
    if ($tokens[0] != $ruser)
        continue;
        
    $itemid = $tokens[1];
    $count = $value;

    $item = Item::fromDb($itemid, true);
    $items[] = [$item, $count];
    $totalCost += $item->salePrice * $count;
}
?>

<h3>Shopping cart</h3>
<div id="order">
    <div id="cart-items">
        <table class="table">
            <!-- image - item name - price - count - buttons -->
            <?php foreach ($items as $key => $value) {
                $item = $value[0];
                $count = $value[1];
                echo $item->buildCartItem($count);
            } ?>
        </table>
    </div>
    <hr>
    <div class="d-flex align-items-end justify-content-between">
        <h5 class="totalCost">
            Total cost: <strong class="totalCostValue"><?= $totalCost ?></strong> UAH
        </h5>
        <form action="/scripts/processOrder.php" method="POST">
            <button class="btn btn-success">Purchase order</button>
        </form>
    </div>
</div>