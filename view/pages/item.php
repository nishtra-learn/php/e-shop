<?php
$id = $_GET["item"];
$item = Item::fromDb($id, true);
?>

<h3><?= $item->itemName ?></h3>
<h5 class="text-success"><?= $item->salePrice ?> UAH</h5>
<div id="gallery">
    <?php foreach ($item->images as $key => $im) : ?>
        <div class="img-container">
            <img src="<?= $im->imagepath ?>" alt="">
        </div>
    <?php endforeach ?>
</div>
<p>
    <?= $item->info ?>
</p>