<h3>Registration</h3>

<?php
$regFail = isset($_SESSION[SES_ERROR]);
$login = $_SESSION[SES_SUCCESS] ?? $_SESSION[SES_ERROR]["login"] ?? "";
$validationErrors = $_SESSION[SES_ERROR]["errors"];
$invalidLogin = $validationErrors["login"];
$invalidPass = $validationErrors["password"];
$invalidPassConfirm = $validationErrors["passwordConfirm"];
$invalidUserpic = $validationErrors["userpic"];
?>

<!-- form processing result message -->
<?php if (isset($_SESSION[SES_SUCCESS])): ?>
    <div class="alert alert-success" role="alert">
        <p>
            User <strong><?= $login ?></strong> registered successfully
        </p>
    </div>
<?php elseif ($regFail): ?>
    <div class="alert alert-danger" role="alert">
        <p>
            Registration failed
        <p>
        <ul>
            <?php if (isset($_SESSION[SES_ERROR]["errors"]["db"])): ?>
            <li><?= $_SESSION[SES_ERROR]["errors"]["db"] ?></li>
            <?php endif ?>
            <?php if (isset($_SESSION[SES_ERROR]["errors"]["saveFile"])): ?>
            <li><?= $_SESSION[SES_ERROR]["errors"]["saveFile"] ?></li>
            <?php endif ?>
        </ul>
    </div>
<?php endif ?>

<!-- registration form -->
<form action="/scripts/processRegistrationForm.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" name="login" id="login" class="form-control <?= $invalidLogin ? "is-invalid" : "" ?>" value="<?= $values["login"] ?>">
        <div class="invalid-feedback" id="loginInvalidFeedback">
            <?= $invalidLogin ? $validationErrors["login"] : "" ?>
        </div>
    </div>
    <div class="form-group">
        <label for="pass">Password</label>
        <input type="password" name="pass" class="form-control <?= $invalidPass ? "is-invalid" : "" ?>">
        <div class="invalid-feedback">
            <?= $invalidPass ? $validationErrors["password"] : "" ?>
        </div>
    </div>
    <div class="form-group">
        <label for="passConfirm">Confirm password</label>
        <input type="password" name="passConfirm" class="form-control <?= $invalidPassConfirm ? "is-invalid" : "" ?>">
        <div class="invalid-feedback">
            <?= $invalidPassConfirm ? $validationErrors["passwordConfirm"] : "" ?>
        </div>
    </div>
    <div class="form-group">
        <div class="form-control" id="userpic-input">
            <label for="passConfirm">Userpic</label>
            <input type="file" name="uploadUserpic" class="form-control-file mb-2" accept=".jpg,.jpeg">
        </div>
        <div class="invalid-feedback">
            <?= $invalidPassConfirm ? $validationErrors["userpic"] : "" ?>
        </div>
    </div>
    <input type="submit" name="regsubmit" value="Register" class="btn btn-primary">
</form>


<?php
unset($_SESSION[SES_SUCCESS]);
unset($_SESSION[SES_ERROR]);
?>