<?php
$categories = Category::getDbEntries();
$err = null;
if ($categories == false) {
    $err = "DB request fail";
}
?>

<h3>Admin panel</h3>

<!-- message -->
<?php if (isset($_SESSION[SES_SUCCESS])): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?= $_SESSION[SES_SUCCESS] ?: "" ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif ?>
<?php if (isset($_SESSION[SES_ERROR]["message"])): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?= $_SESSION[SES_ERROR]["message"] ?: "" ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif ?>

<div class="card">
    <div class="card-header">
        Add new category
    </div>
    <div class="card-body">
        <form action="/scripts/addCategory.php" method="post">
            <div class="form-group">
                <label>Category name</label>
                <input type="text" name="categoryName" class="form-control <?= $_SESSION[SES_ERROR]["category"] ? "is-invalid" : "" ?>">
                <div class="invalid-feedback">
                    <?= $_SESSION[SES_ERROR]["category"] ?: "" ?>
                </div>
            </div>
            <input type="submit" value="Create" class="btn btn-primary">
        </form>
    </div>
</div>

<div class="card mt-3">
    <div class="card-header">
        Add new item
    </div>
    <div class="card-body">
        <form action="/scripts/addItem.php" method="post" enctype="multipart/form-data">
            <!-- item name -->
            <div class="form-group">
                <label for="itemName">Item Name</label>
                <input type="text" name="itemName" id="itemName" class="form-control <?= $_SESSION[SES_ERROR]["itemName"] ? "is-invalid" : "" ?>">
                <div class="invalid-feedback">
                    <?= $_SESSION[SES_ERROR]["itemName"] ?: "" ?>
                </div>
            </div>
            <!-- category -->
            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="form-control">
                    <?php foreach ($categories as $cat): ?>
                        <option value="<?= $cat->id ?>">
                            <?= $cat->category ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
            <!-- price -->
            <div class="form-group">
                <label for="price">Price</label>
                <input type="number" name="price" id="price" class="form-control <?= $_SESSION[SES_ERROR]["price"] ? "is-invalid" : "" ?>" min="0">
                <div class="invalid-feedback">
                    <?= $_SESSION[SES_ERROR]["price"] ?: "" ?>
                </div>
            </div>
            <!-- salePrice -->
            <div class="form-group">
                <label for="salePrice">Sale Price</label>
                <input type="number" name="salePrice" id="salePrice" class="form-control <?= $_SESSION[SES_ERROR]["salePrice"] ? "is-invalid" : "" ?>" min="0">
                <div class="invalid-feedback">
                    <?= $_SESSION[SES_ERROR]["salePrice"] ?: "" ?>
                </div>
            </div>
            <!-- info -->
            <div class="form-group">
                <label for="info">Info</label>
                <textarea name="itemInfo" id="itemInfo" rows="3" class="form-control <?= $_SESSION[SES_ERROR]["itemInfo"] ? "is-invalid" : "" ?>"></textarea>
                <div class="invalid-feedback">
                    <?= $_SESSION[SES_ERROR]["itemInfo"] ?: "" ?>
                </div>
            </div>
            <!-- image upload -->
            <div class="form-group">
                <div class="form-control <?= $_SESSION[SES_ERROR]["itemImages"] ? "is-invalid" : "" ?>" id="image-input">
                    <label>Images</label>
                    <input type="file" name="uploadImages[]" class="form-control-file mb-2" accept=".jpg,.jpeg" multiple>
                </div>
                <div class="invalid-feedback">
                    <?= $_SESSION[SES_ERROR]["itemImages"] ?: "" ?>
                </div>
            </div>
            <input type="submit" value="Add" class="btn btn-primary">
        </form>
    </div>
</div>


<?php
unset($_SESSION[SES_ERROR]);
unset($_SESSION[SES_SUCCESS]);
?>