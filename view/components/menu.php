<?php
function createMenuItem($PAGE, $text) {
    ob_start();
    ?>
    <li class="nav-item <?php if ($_GET["page"] == $PAGE) echo "active" ?>">
        <a href="index.php?page=<?php echo $PAGE ?>" class="nav-link"><?php echo $text ?></a>
    </li>
    <?php
    $elem = ob_get_contents();
    ob_end_clean();
    return $elem;
}
?>

<div class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-menu"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav-menu">
        <ul class="navbar-nav mr-auto">
            <?php echo createMenuItem(PageMap::CATALOG, "Catalog"); ?>
            <?php echo createMenuItem(PageMap::CART, "Shopping Cart"); ?>
            <?php echo createMenuItem(PageMap::REGISTRATION, "Registration"); ?>
            <?php echo createMenuItem(PageMap::ADMIN_PANEL, "Admin Panel"); ?>
        </ul>
        <hr>
        
        <?php 
        if (!isset($_SESSION[SES_RUSER]) && !isset($_SESSION[SES_RADMIN])) {
            include($_SERVER["DOCUMENT_ROOT"] . "/view/components/login_form.php");
        }
        else {
            include($_SERVER["DOCUMENT_ROOT"] . "/view/components/logout_form.php");
        }
        ?>
    </div>
</div>