<?php
if (isset($_POST["log-out"])) {
    unset($_SESSION[SES_RUSER]);
    unset($_SESSION[SES_RADMIN]);
    unset($_SESSION[SES_RUSER_ID]);

    if ($_GET["page"] == PageMap::ADMIN_PANEL || $_GET["page"] == PageMap::ERROR) {
        // redirect to registration page if the current page is restricted to admin
        Helpers::scriptRedirect("/index.php?page=".PageMap::REGISTRATION, true);
    }
    else {
        Helpers::scriptRedirectToCurrent();
    }
}

$username = "";
if (isset($_SESSION[SES_RUSER])) {
    $username = $_SESSION[SES_RUSER];
}
else if (isset($_SESSION[SES_RADMIN])) {
    $username = $_SESSION[SES_RADMIN];
}
?>

<form action="" method="post" class="form-inline my-2 my-md-0">
    <span>Hello, <strong class="mr-2"><?php echo $username ?></strong></span>
    <input type="submit" name="log-out" value="Log Out" class="btn btn-outline-primary">
</form>