<?php
if (isset($_POST["log-in"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    $user = Customer::authenticate($username, $password); // get user from DB
    
    $invalidCredentials = false;
    if ($user == null) {
        $invalidCredentials = true;
    }
    else {
        if ($user->roleid == ROLE_ID_CUSTOMER) {
            $_SESSION[SES_RUSER] = $user->login;
        }
        else if ($user->roleid == ROLE_ID_ADMIN) {
            $_SESSION[SES_RADMIN] = $user->login;
        }
        $_SESSION[SES_RUSER_ID] = $user->id; // customer id is needed when processing a shipping order and it's better to keep it in session than searching DB by login

        // redirect to the same page (or main page if the user logged in on the error page)
        if ($_GET["page"] == PageMap::ERROR) {
            $redirectPath = "/index.php?page=" . PageMap::CATALOG;
            Helpers::scriptRedirect($redirectPath, true);
        }
        else {
            Helpers::scriptRedirectToCurrent();
        }
    }
}
?>

<form action="" method="post" class="form-inline my-2 my-md-0">
    <input type="text" name="username" placeholder="Login" required
        class="form-control mb-2 mr-sm-2 <?= ($invalidCredentials) ? "is-invalid" : "" ?>">
    <input type="password" name="password" placeholder="Password" required
        class="form-control mb-2 mr-sm-2 <?= ($invalidCredentials) ? "is-invalid" : "" ?>">
    <button type="submit" name="log-in" class="btn btn-outline-primary mb-2">Sign In</button>
</form>