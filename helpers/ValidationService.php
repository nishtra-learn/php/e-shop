<?php
class ValidationService {
    const MAX_USERPIC_FILE_SIZE_KB = 1000;
    const MAX_ITEM_IMAGE_FILE_SIZE_KB = 5000;

    public static function login($login) {
        if ($login == null || $login == "")
            return "This field is required";
        if (strlen($login) < 2 || strlen($login) > 20)
            return "Login should be between 2 and 20 characters long";
        
        return null;
    }

    public static function password($password) {
        if ($password == null || $password == "")
            return "This field is required";
        if (strlen($password) < 3 || strlen($password) > 128)
            return "Password can't be shorter than 3 and longer than 128 characters";

        return null;
    }

    public static function passwordConfirm($password, $passwordConfirm) {
        if ($passwordConfirm == null || $passwordConfirm == "") 
            return "This field is required";
        if ($password != $passwordConfirm)
            return "Passwords do not match";
        
        return null;
    }

    public static function userpic($userpicFile) {
        $filenameLen = strlen($userpicFile["name"]);
        $timestampLen = strlen(time());
        $imageFolderPathLen = strlen($_SERVER["DOCUMENT_ROOT"] . "/images");
        if ($filenameLen + $timestampLen + $imageFolderPathLen > 256)
            return "Filename too long";
        if ($userpicFile["size"] / 1000 > ValidationService::MAX_USERPIC_FILE_SIZE_KB)
            return "Cannot upload files larger than " . ValidationService::MAX_USERPIC_FILE_SIZE_KB . "KB";
        if ($userpicFile["error"] != UPLOAD_ERR_OK && $userpicFile["error"] != UPLOAD_ERR_NO_FILE)
            return "File upload error " . $userpicFile["error"];
        
        return null;
    }

    public static function registrationForm($login, $password, $passwordConfirm, $userpicFile) {
        $errors = [];

        if ($err = ValidationService::login($login))
            $errors["login"] = $err;
        if ($err = ValidationService::password($password))
            $errors["password"] = $err;
        if ($err = ValidationService::passwordConfirm($password, $passwordConfirm))
            $errors["passwordConfirm"] = $err;
        if ($err = ValidationService::userpic($userpicFile))
            $errors["userpic"] = $err;

        return $errors;
    }

    public static function itemName($itemName) {
        if ($itemName == "")
            return "This field is required";
        if (strlen($itemName) > 64)
            return "Item's name can not be longer than 64 characters";

        return null;
    }

    public static function itemInfo($iteminfo) {
        if (strlen($iteminfo) > 256)
            return "Item's description can not be longer than 64 characters";

        return null;
    }

    public static function itemImages($files) {
        foreach ($files["name"] as $key => $value) {
            if ($files["error"][$key] != UPLOAD_ERR_OK && $files["error"][$key] != UPLOAD_ERR_NO_FILE)
                return ("File upload error " . $files["error"][$key]);

            if ($files["size"][$key] > ValidationService::MAX_ITEM_IMAGE_FILE_SIZE_KB * 1000)
                return "Max allowed file size is " . ValidationService::MAX_ITEM_IMAGE_FILE_SIZE_KB . " KB";

            $pathLen = strlen($_SERVER["DOCUMENT_ROOT"] . "/images" . $files["name"][$key] . time());
            if ($pathLen > 255)
                return "One (or several) filename is too long";
        }

        return null;
    }

    public static function itemAddForm($itemName, $iteminfo, $imageFiles) {
        $errors = [];

        if ($err = ValidationService::itemName($itemName))
            $errors["itemName"] = $err;
        if ($err = ValidationService::itemInfo($iteminfo))
            $errors["iteminfo"] = $err;
        if ($err = ValidationService::itemImages($imageFiles))
            $errors["itemImages"] = $err;

        return $errors;
    }
}
