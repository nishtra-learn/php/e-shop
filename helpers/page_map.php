<?php

class PageModel
{
    public $title;
    public $page_file;

    public function __construct($title, $page_file)
    {
        $this->title = $title;
        $this->page_file = $page_file;
    }
}

class PageMap { 
    public const REGISTRATION = "registration";
    public const CATALOG = "catalog";
    public const CART = "cart";
    public const ADMIN_PANEL = "admin";
    public const ITEM = "item";
    public const ERROR = "error";

    public static function Map($PAGE) {
        switch ($PAGE) {
            case self::REGISTRATION :
                return new PageModel("Registration", $_SERVER["DOCUMENT_ROOT"] . "/view/pages/registration.php");
            case self::CATALOG :
                return new PageModel("Catalog", $_SERVER["DOCUMENT_ROOT"] . "/view/pages/catalog.php");
            case self::CART :
                return new PageModel("Shopping Cart", $_SERVER["DOCUMENT_ROOT"] . "/view/pages/cart.php");
            case self::ADMIN_PANEL :
                return new PageModel("Admin Panel", $_SERVER["DOCUMENT_ROOT"] . "/view/pages/admin.php");
            case self::ITEM :
                return new PageModel("Item", $_SERVER["DOCUMENT_ROOT"] . "/view/pages/item.php");
            case self::ERROR :
                return new PageModel("Error", $_SERVER["DOCUMENT_ROOT"] . "/view/pages/error.php");
            default:
                return null;
        }
    }
}