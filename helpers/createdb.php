<?php
include_once("helpers.php");
$pdo = Helpers::connect();

$sqlRoles = "CREATE TABLE IF NOT EXISTS Roles(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    role varchar(32) NOT NULL UNIQUE
    ) DEFAULT CHARSET 'UTF8'";

$sqlCustomers = "CREATE TABLE IF NOT EXISTS Customers(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    login varchar(32) NOT NULL UNIQUE,
    `password` varchar(128),
    roleid int,
    FOREIGN KEY(roleid) REFERENCES Roles(id) ON UPDATE CASCADE,
    imagepath varchar(256),
    discount int DEFAULT 0,
    total double
    ) DEFAULT CHARSET 'UTF8'";

$sqlCategories = "CREATE TABLE IF NOT EXISTS Categories(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    category varchar(32) NOT NULL UNIQUE
    ) DEFAULT CHARSET 'UTF8'";

$sqlItems = "CREATE TABLE IF NOT EXISTS Items(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    itemName varchar(64) NOT NULL,
    categoryid int,
    FOREIGN KEY(categoryid) REFERENCES Categories(id) ON UPDATE CASCADE,
    price double,
    salePrice double,
    info varchar(256),
    rating double,
    `action` int
    ) DEFAULT CHARSET 'UTF8'";

$sqlImages = "CREATE TABLE IF NOT EXISTS Images(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    itemid int,
    FOREIGN KEY(itemid) REFERENCES Items(id) ON DELETE CASCADE,
    imagepath varchar(256)
    ) DEFAULT CHARSET 'UTF8'";

$sqlSales = "CREATE TABLE IF NOT EXISTS Sales(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    itemid int,
    FOREIGN KEY(itemid) REFERENCES Items(id) ON UPDATE CASCADE,
    customerid int,
    FOREIGN KEY(customerid) REFERENCES Customers(id) ON UPDATE CASCADE,
    quantity int,
    `date` date
    ) DEFAULT CHARSET 'UTF8'";

$pdo->exec($sqlRoles);
$pdo->exec($sqlCustomers);
$pdo->exec($sqlCategories);
$pdo->exec($sqlItems);
$pdo->exec($sqlImages);
$pdo->exec($sqlSales);
echo "All tables created";
echo "<br>";

$psPopulateRoles = $pdo->prepare("INSERT INTO Roles(`role`) VALUES (?), (?)");
$psPopulateRoles->execute(array("admin", "customer"));
echo "Roles populated";