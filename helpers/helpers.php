<?php
class Helpers {
    public static function connect(
        $host = "localhost:3306",
        $dbname = "storedb",
        $user = "root",
        $password = "root"
    )
    {
        $connectionStr = "mysql:host=" . $host . ";dbname=" . $dbname . ";charset=utf8";
        $options = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            PDO::ATTR_EMULATE_PREPARES => false
        );

        try {
            $pdo = new PDO($connectionStr, $user, $password, $options);
            return $pdo;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            return false;
        }
    }


    public static function registerUser($login, $password, $userpicPath = null) {
        $passwordHash = md5($password);
        $user = new Customer($login, $passwordHash, $userpicPath);
        $res = $user->intoDb();
        return $res;
    }


    public static function scriptRedirect($redirectPath, $replaceCurrent = false) {
        echo "<script>";
        echo $replaceCurrent 
                ? "window.location.replace('$redirectPath');" 
                : "window.location.href = '$redirectPath';";
        echo "</script>";
        exit();
    }


    public static function scriptRedirectToCurrent() {
        echo "<script>";
        echo "window.location.replace(window.location.href);";
        echo "</script>";
        exit();
    }


    public static function headerRedirect($redirectPath) {
        header("Location: $redirectPath", true, 303);
        exit();
    }
}