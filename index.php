<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/constants.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/helpers/page_map.php");
include_once($_SERVER["DOCUMENT_ROOT"] . "/models/models_import.php");

session_start();
$ruser = $_SESSION[SES_RUSER] ?? $_SESSION[SES_RADMIN] ?? "-"; // at least some value is needed for cookiee to be saved
$ruser = rawurlencode($ruser);
setcookie("ruser", $ruser, time() + 60*60*24); // cookie keeping the current logged-in user

$page = strtolower($_GET["page"]);
if (PageMap::Map($page) === null) {
    if ($_SERVER["REQUEST_URI"] == "/index.php" || $_SERVER["REQUEST_URI"] == "/") {
        $page = PageMap::CATALOG;
    }
    else {
        header("Location: /index.php?page=" . PageMap::ERROR);
        $_SESSION[SES_ERROR] = "Page '$page' can not be accessed or doesn't exist";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= PageMap::Map($page)->title ?></title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css">
</head>
<body>
    <header>
        <?php include($_SERVER["DOCUMENT_ROOT"] . "/view/components/menu.php") ?>
    </header>
    <div class="container main">
        <?php include_once(PageMap::Map($page)->page_file) ?>
    </div>
    <footer>
        <hr>
        IT STEP Academy Kryvyi Rih &copy;
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>